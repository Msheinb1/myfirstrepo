all: test

test: test.o
	g++ -std=c++11 -g test.o -o test

test.o: HelloWorld.cpp
	g++ -std=c++11 -Wall -Wextra -pedantic -c -g HelloWorld.cpp -o test.o